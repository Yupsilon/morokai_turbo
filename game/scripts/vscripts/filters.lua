---------------------------------------------------------------------------
--	ModifierGainedFilter
--  *entindex_parent_const
--	*entindex_ability_const
--	*entindex_caster_const
--	*name_const
--	*duration
---------------------------------------------------------------------------

function CJungleSpirits:ModifierGainedFilter( filterTable )
	if filterTable["entindex_parent_const"] == nil then 
		return true
	end

 	if filterTable["name_const"] == nil then
		return true
	end

	local szBuffName = filterTable["name_const"]
	local hParent = EntIndexToHScript( filterTable["entindex_parent_const"] )
	local hBuff = nil
	if hParent ~= nil then
		hBuff = hParent:FindModifierByName( szBuffName )
	end

	if hBuff == nil then
		return true
	end

	if hParent == self._hRubick then
		if 	szBuffName == "modifier_stunned" or
			szBuffName == "modifier_sheepstick_debuff" or
			szBuffName == "modifier_bloodthorn_debuff" or
			szBuffName == "modifier_orchid_malevolence_debuff" or
			szBuffName == "modifier_bashed" or
			szBuffName == "modifier_silence" or 
			szBuffName == "modifier_rooted" or 
			szBuffName == "modifier_faceless_void_timelock_freeze" or
			szBuffName == "modifier_dark_willow_debuff_fear" then
			return false
		end
	end

	return true
end

--------------------------------------------------------------------------------

function CJungleSpirits:ModifyExperienceFilter( filterTable )
	--printf( "ModifyExperienceFilter - filterTable:" )
	--PrintTable( filterTable )

	local player = PlayerResource:GetPlayer( filterTable[ "player_id_const" ] )
	if player == nil then
		return true
	end

	--printf( "  before - filterTable[ \"experience\" ] == %d", filterTable[ "experience" ] )

	filterTable[ "experience" ] = filterTable[ "experience" ] * HERO_EXPERIENCE_GAIN_MULTIPLIER

	--printf( "  after - filterTable[ \"experience\" ] == %d", filterTable[ "experience" ] )
	
	if GameRules:GetGameTime()>TURNAROUND_TIME then
		filterTable[ "experience" ] = filterTable[ "experience" ] * TURNAROUND_EXP
	end

	return true
end

--------------------------------------------------------------------------------

function CJungleSpirits:ModifyGoldFilter( filterTable )
	--printf( "ModifyGoldFilter - filterTable:" )
	--PrintTable( filterTable )

	local player = PlayerResource:GetPlayer( filterTable[ "player_id_const" ] )
	print(player)
	--local hero = player:GetAssignedHero()
	if player == nil then
		return true
	end
 
	print("player not nil")
	print(filterTable[ "reason_const" ].." "..DOTA_ModifyGold_CreepKill.." "..filterTable[ "gold" ])
	--printf( "  before - filterTable[ \"gold\" ] == %d", filterTable[ "gold" ] )
	
	if filterTable[ "reason_const" ] == DOTA_ModifyGold_GameTick or filterTable[ "reason_const" ] == DOTA_ModifyGold_CreepKill then
		filterTable[ "gold" ] = filterTable[ "gold" ] * HERO_GOLD_GAIN_MULTIPLIER
	end

	
	if GameRules:GetGameTime()>TURNAROUND_TIME then
		filterTable[ "gold" ] = filterTable[ "gold" ] * TURNAROUND_GOLD
	end
	--printf( "  before - filterTable[ \"gold\" ] == %d", filterTable[ "gold" ] )

	return true
end
