modifier_spirit_gem = class({})

--------------------------------------------------------------------------------

function modifier_spirit_gem:IsHidden()
	return false
end

--------------------------------------------------------------------------------

function modifier_spirit_gem:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_STATS_STRENGTH_BONUS,
		MODIFIER_PROPERTY_STATS_AGILITY_BONUS,
		MODIFIER_PROPERTY_STATS_INTELLECT_BONUS
	}

	return funcs
end

--------------------------------------------------------------------------------

function modifier_spirit_gem:GetModifierBonusStats_Strength( params )

	if not self:GetParent():IsHero() or self:GetParent():GetPrimaryAttribute() == 0 then
		return 0
	end
	return self:GetStackCount()
end
end

--------------------------------------------------------------------------------

function modifier_spirit_gem:GetModifierBonusStats_Agility( params )

	if not self:GetParent():IsHero() or self:GetParent():GetPrimaryAttribute() == 1 then
		return 0
	end
	return self:GetStackCount()
end

--------------------------------------------------------------------------------

function modifier_spirit_gem:GetModifierBonusStats_Intellect( params )

	if not self:GetParent():IsHero() or self:GetParent():GetPrimaryAttribute() == 2 then
		return 0
	end
	return self:GetStackCount()
end
end
	
--------------------------------------------------------------------------------